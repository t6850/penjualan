<?php

use App\Http\Controllers\LoginController;
use App\Http\Controllers\ProdukController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->post('/user', function (Request $request) {
    return $request->user();
});

// login service
Route::post('/loginservice/login', [LoginController::class, 'login']);
Route::middleware('auth:sanctum')->post('/loginservice/logout', [LoginController::class, 'logout']);
Route::middleware('auth:sanctum')->post('/loginservice/updateuserauth', [LoginController::class, 'updateUserAuth']);
// produk service
Route::post('/produkservice/getproduklist', [ProdukController::class, 'getProdukList']);
Route::post('/produkservice/storeproduk', [ProdukController::class, 'storeProduk']);
Route::post('/produkservice/updateproduk', [ProdukController::class, 'updateProduk']);
Route::post('/produkservice/deleteprodukbycode', [ProdukController::class, 'deleteProdukByKode']);
Route::post('/produkservice/getprodukbykode', [ProdukController::class, 'getProdukByKode']);