<?php

namespace App\Exceptions;

use App\Constants\ErrorCodeConstant;
use App\Constants\ResponseStatusCodeConstant;
use Exception;

class LoginFailedException extends Exception
{
    public static function execute($message)
    {
        return response()->json(CommonException::execute(ErrorCodeConstant::LOGIN_FAILED, $message), ResponseStatusCodeConstant::NOT_FOUND);
    }
}
