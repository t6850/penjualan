<?php

namespace App\Exceptions;

use App\Helpers\DateConvertUtils;
use DateTime;

class CommonException
{
    public static function execute($errorCode, $message)
    {
        return [
            "errorCode" => $errorCode,
            "message" => $message,
            "sourceSystem" => "Digital Inventory",
            "timestamp" => DateConvertUtils::toTimestamp(new DateTime())
        ];
    }
}
