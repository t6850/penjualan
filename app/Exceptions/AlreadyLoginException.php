<?php

namespace App\Exceptions;

use App\Constants\ErrorCodeConstant;
use App\Constants\ResponseStatusCodeConstant;
use Exception;

class AlreadyLoginException extends Exception
{
    public static function execute($message)
    {
        return response()->json(CommonException::execute(ErrorCodeConstant::ALREADY_LOGIN, $message), ResponseStatusCodeConstant::BAD_REQUEST);
    }
}
