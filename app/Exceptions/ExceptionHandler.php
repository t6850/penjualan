<?php

namespace App\Exceptions;

use App\Constants\ErrorCodeConstant;
use App\Constants\ResponseStatusCodeConstant;
use Exception;

class ExceptionHandler extends Exception
{
    public static function execute($message)
    {
        return response()->json(CommonException::execute(ErrorCodeConstant::SOMETHING_WENT_WRONG, $message), ResponseStatusCodeConstant::INTERNAL_SERVER_ERROR);
    }
}
