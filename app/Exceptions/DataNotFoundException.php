<?php

namespace App\Exceptions;

use App\Constants\ErrorCodeConstant;
use App\Constants\ResponseStatusCodeConstant;
use Exception;

class DataNotFoundException extends Exception
{
    public static function execute($message)
    {
        return response()->json(CommonException::execute(ErrorCodeConstant::DATA_NOT_FOUND, $message), ResponseStatusCodeConstant::NOT_FOUND);
    }
}
