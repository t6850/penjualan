<?php

namespace App\Exceptions;

use App\Constants\ErrorCodeConstant;
use App\Constants\ResponseStatusCodeConstant;
use Exception;

class BadRequestException extends Exception
{
    public static function execute($message)
    {
        return response()->json(CommonException::execute(ErrorCodeConstant::BAD_REQUEST, $message), ResponseStatusCodeConstant::BAD_REQUEST);
    }
}
