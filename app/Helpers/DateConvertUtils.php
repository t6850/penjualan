<?php

namespace App\Helpers;

use Carbon\Carbon;

class DateConvertUtils
{
    private const DATE_FORMAT = "Y-m-d H:i:s";
    public static function toTimestamp($dateTime)
    {
        if (!empty($dateTime)) {
            return Carbon::parse($dateTime)->timestamp;
        }
    }

    public static function epochToDateTime($epoch)
    {
        if (!empty($epoch)) {
            return date(DateConvertUtils::DATE_FORMAT, $epoch);
        }
    }
}
