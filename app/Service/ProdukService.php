<?php

namespace App\Service;

use App\Constants\ErrorMessageConstant;
use App\Exceptions\BadRequestException;
use App\Exceptions\DataNotFoundException;
use App\Exceptions\ExceptionHandler;
use App\Http\Request\GetProdukByKodeRequest;
use App\Http\Request\StoreProdukRequest;
use App\Repository\ProdukRepository;
use Exception;
use Illuminate\Support\Facades\Log;

class ProdukService
{
    public function getProdukList()
    {
        Log::info('start getProdukList');
        try {
            $produkList = ProdukRepository::findAll();
            if ($produkList->count() == 0) {
                Log::error(ErrorMessageConstant::DATA_NOT_FOUND);
                return DataNotFoundException::execute(ErrorMessageConstant::DATA_NOT_FOUND);
            }
            Log::info("Number of parameter data : ". $produkList->count());
            return $produkList;
        } catch (Exception $e) {
            Log::error("Error Exception : ". $e);
            return ExceptionHandler::execute($e->getMessage());
        } finally {
            Log::info("end getProdukList");
        }
    }

    public function storeProduk(StoreProdukRequest $request)
    {
        Log::info("start storeProduk");
        Log::info("request : ". json_encode($request->toArray()));
        try {
            if (empty($request->getNama()) || empty($request->getMerk()) || empty($request->getHarga()) ) {
                Log::error(ErrorMessageConstant::BAD_REQUEST);
                return BadRequestException::execute(ErrorMessageConstant::BAD_REQUEST);
            }
            $request->setKode(uniqid());
            $storeProduk = ProdukRepository::save($request);
            if (empty($storeProduk)) {
                return ExceptionHandler::execute(ErrorMessageConstant::SOMETHING_WENT_WRONG);
            }
            return $storeProduk;
        } catch (Exception $e) {
            Log::error("Error Exception : ". $e);
            return ExceptionHandler::execute($e->getMessage());
        } finally {
            Log::info("end storeProduk");
        }
    }

    public function updateProduk(StoreProdukRequest $request)
    {
        Log::info("start updateProduk");
        Log::info("request : ". json_encode($request->toArray()));
        try {
            if (empty($request->getKode()) || empty($request->getNama()) || empty($request->getMerk()) || empty($request->getHarga())) {
                Log::error(ErrorMessageConstant::BAD_REQUEST);
                return BadRequestException::execute(ErrorMessageConstant::BAD_REQUEST);
            }
            $produkOptional = ProdukRepository::findByKode($request->getKode());
            if ($produkOptional->count() == 0) {
                Log::error(ErrorMessageConstant::DATA_NOT_FOUND);
                return DataNotFoundException::execute(ErrorMessageConstant::DATA_NOT_FOUND);
            }
            $produkData = json_encode($produkOptional[0]);
            $produkData = json_decode($produkData);
            if ($request->getNama() == $produkData->nama && $request->getMerk() == $produkData->merk && $request->getHarga() == $produkData->harga) {
                return [
                    "data" => $produkOptional
                ];
            }
            $updateProduk = ProdukRepository::update($request);
            if (empty($updateProduk)) {
                Log::error(ErrorMessageConstant::SOMETHING_WENT_WRONG);
                return ExceptionHandler::execute(ErrorMessageConstant::SOMETHING_WENT_WRONG);
            }
            return $updateProduk;
        } catch (Exception $e) {
            Log::error("Error Exception : ". $e);
            return ExceptionHandler::execute($e->getMessage());
        } finally {
            Log::info("end updateProduk");
        }
    }

    public function deleteProdukByKode(GetProdukByKodeRequest $request)
    {
        Log::info("start deleteProdukByKode");
        Log::info("request : ". json_encode($request->toArray()));
        try {
            if (empty($request->getKode())) {
                Log::error(ErrorMessageConstant::BAD_REQUEST);
                return BadRequestException::execute(ErrorMessageConstant::BAD_REQUEST);
            }
            $produkOptional = ProdukRepository::findByKode($request->getKode());
            if ($produkOptional->count() == 0) {
                Log::error(ErrorMessageConstant::DATA_NOT_FOUND);
                return DataNotFoundException::execute(ErrorMessageConstant::DATA_NOT_FOUND);
            }
            $deleteProduk = ProdukRepository::delete($request);
            return [
                "success" => $deleteProduk
            ];
        } catch (Exception $e) {
            Log::error("Error Exception : ". $e);
            return ExceptionHandler::execute($e->getMessage());
        } finally {
            Log::info("end deleteProdukByKode");
        }
    }

    public function getProdukByKode(GetProdukByKodeRequest $request)
    {
        Log::info("start getProdukByKode");
        Log::info("request : ". json_encode($request->toArray()));
        try {
            if (empty($request->getKode())) {
                Log::error(ErrorMessageConstant::BAD_REQUEST);
                return BadRequestException::execute(ErrorMessageConstant::BAD_REQUEST);
            }
            $produkOptional = ProdukRepository::findByKode($request->getKode());
            if ($produkOptional->count() == 0) {
                Log::error(ErrorMessageConstant::DATA_NOT_FOUND);
                return DataNotFoundException::execute(ErrorMessageConstant::DATA_NOT_FOUND);
            }
            return $produkOptional;
        } catch (Exception $e) {
            Log::error("Error Exception : ". $e);
            return ExceptionHandler::execute($e->getMessage());
        } finally {
            Log::info("end getProdukByKode");
        }
    }
}
