<?php

namespace App\Service;

use App\Constants\ErrorMessageConstant;
use App\Exceptions\AlreadyLoginException;
use App\Exceptions\BadRequestException;
use App\Exceptions\DataNotFoundException;
use App\Exceptions\ExceptionHandler;
use App\Exceptions\LoginFailedException;
use App\Http\Request\GetLoginRequest;
use App\Repository\PersonalAccessTokensRepository;
use App\Repository\UserRepository;
use DateTime;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class LoginService
{
    public function login(GetLoginRequest $request)
    {
        Log::info("start login");
        Log::info("request : ". json_encode($request->toArray()));
        try {
            if (empty($request->getEmail()) || empty($request->getPassword())) {
                Log::error(ErrorMessageConstant::BAD_REQUEST);
                return BadRequestException::execute(ErrorMessageConstant::BAD_REQUEST);
            }
            $userOptional = UserRepository::findByEmail($request->getEmail());
            if (!$userOptional || !Hash::check($request->getPassword(), $userOptional->password)) {
                return LoginFailedException::execute(ErrorMessageConstant::LOGIN_FAILED);
            }

            $personalAccessTokenOptional = PersonalAccessTokensRepository::findByTokenableId($userOptional->id);
            // $token = null;
            // if ($personalAccessTokenOptional->count() != 0) {
            //     Log::info("user token found :". $personalAccessTokenOptional);
            //     $personalTokenData = json_decode($personalAccessTokenOptional[0]);
            //     $createdDate = $personalTokenData->created_at;
            //     $isValidToken = $this->validateToken($createdDate);
            //     if ($isValidToken) {
            //         return AlreadyLoginException::execute(ErrorMessageConstant::ALREADY_LOGIN);
            //     }
            //     $userTokenId = $personalTokenData->tokenable_id;
            //     PersonalAccessTokensRepository::deleteByTokenableId($userTokenId);
            // }
            $token = $userOptional->createToken('ApiToken')->plainTextToken;
            $response = [
                'user'      => $userOptional,
                'token'     => $token
            ];

            return [
                "data" => $response
            ];
        } catch (Exception $e) {
            Log::error("Error Exception : ". $e);
            return ExceptionHandler::execute($e->getMessage());
        } finally {
            Log::info("end login");
        }
    }

    public function logout(Request $request)
    {
        $request->user()->currentAccessToken()->delete();
        return [
            "success" => true
        ];
    }

    public function updateUserAuth(Request $request)
    {
        $user = $request->user();
        $getLoginRequest = new GetLoginRequest();
        $getLoginRequest->setEmail($user->email);
        $this->logout($request);
        $userOptional = UserRepository::findByEmail($getLoginRequest->getEmail());
        if (empty($userOptional)) {
            Log::error(ErrorMessageConstant::DATA_NOT_FOUND);
            return DataNotFoundException::execute(ErrorMessageConstant::DATA_NOT_FOUND);
        }
        $token = $userOptional->createToken('ApiToken', ["server:update"])->plainTextToken;
        $response = [
            'user'      => $userOptional,
            'token'     => $token
        ];

        return [
            "data" => $response
        ];
    }

    private function validateToken($createdDate)
    {
        $createdAt = date_create($createdDate);
        date_add($createdAt, date_interval_create_from_date_string('7 hours'));
        $createdAtString = $createdAt->format('Y-m-d H:i:s');
        $expiredToken = date_create($createdAtString);
        date_add($expiredToken, date_interval_create_from_date_string('5 minutes'));
        $expiredTokenString = $expiredToken->format('Y-m-d H:i:s');

        $dateNow = new DateTime(now());
        $dateNow = $dateNow->getTimestamp();
        $expiredToken = new DateTime($expiredTokenString);
        $expiredToken = $expiredToken->getTimestamp();
        $expiredTokenCal = $expiredToken - $dateNow;
        if ($expiredTokenCal < 0) {
            return false;
        }
        return true;
    }
}