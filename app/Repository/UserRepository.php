<?php

namespace App\Repository;

use App\Models\User;

class UserRepository
{
    public static function findByEmail($email)
    {
        return User::where('email', $email)->first();
    }
}
