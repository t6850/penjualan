<?php

namespace App\Repository;

use App\Http\Request\GetProdukByKodeRequest;
use App\Http\Request\StoreProdukRequest;
use App\Http\Resources\ProdukResource;
use App\Models\Produk;

class ProdukRepository
{
    public static function findAll()
    {
        return ProdukResource::collection(Produk::orderBy('nama','asc')->get());
    }

    public static function save(StoreProdukRequest $request)
    {
        $produk = new Produk();
        $produk->kode = $request->getKode();
        $produk->nama = $request->getNama();
        $produk->merk = $request->getMerk();
        $produk->harga = $request->getHarga();
        $storeProduk = $produk->save();
        if ($storeProduk) {
            return ProdukResource::collection(Produk::find($produk));
        }
        return null;
    }

    public static function update(StoreProdukRequest $request)
    {
        $updateProduk = Produk::where('kode', $request->getKode())->update([
            'nama' => $request->getNama(),
            'merk' => $request->getMerk(),
            'harga' => $request->getHarga()
        ]);
        if ($updateProduk) {
            return ProdukResource::collection(Produk::where('kode', $request->getKode())->get());
        }
        return null;
    }

    public static function findByKode($kode)
    {
        return ProdukResource::collection(Produk::where('kode', $kode)->get());
    }

    public static function delete(GetProdukByKodeRequest $request)
    {
        $deleteProduk = Produk::where('kode', $request->getKode())->delete();
        if ($deleteProduk) {
            return true;
        }
        return false;
    }
}
