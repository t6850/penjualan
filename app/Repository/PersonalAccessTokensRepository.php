<?php

namespace App\Repository;

use Laravel\Sanctum\PersonalAccessToken;

class PersonalAccessTokensRepository
{
    public static function findByTokenableId($tokenableId)
    {
        return PersonalAccessToken::where('tokenable_id', $tokenableId)->get();
    }

    public static function deleteByTokenableId($tokenableId)
    {
        return PersonalAccessToken::where('tokenable_id', $tokenableId)->delete();
    }
}
