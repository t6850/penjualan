<?php

namespace App\Http\Request;

class GetProdukByKodeRequest
{
    private $kode;

    public function getKode()
    {
        return $this->kode;
    }

    public function setKode($kode)
    {
        $this->kode = $kode;
    }

    public function toArray()
    {
        return [
            'kode' => $this->getKode()
        ];
    }
}
