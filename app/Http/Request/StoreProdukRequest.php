<?php

namespace App\Http\Request;

class StoreProdukRequest
{
    private $kode;
    private $nama;
    private $merk;
    private $harga;

    public function getKode()
    {
        return $this->kode;
    }

    public function setKode($kode)
    {
        $this->kode = $kode;
    }

    public function getNama()
    {
        return $this->nama;
    }

    public function setNama($nama)
    {
        $this->nama = $nama;
    }

    public function getMerk()
    {
        return $this->merk;
    }

    public function setMerk($merk)
    {
        $this->merk = $merk;
    }

    public function getHarga()
    {
        return $this->harga;
    }

    public function setHarga($harga)
    {
        $this->harga = $harga;
    }

    public function toArray()
    {
        return [
            'kode' => $this->getKode(),
            'nama' => $this->getNama(),
            'merk' => $this->getMerk(),
            'harga' => $this->getHarga()
        ];
    }
}
