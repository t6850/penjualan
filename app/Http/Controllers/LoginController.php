<?php

namespace App\Http\Controllers;

use App\Http\Request\GetLoginRequest;
use App\Service\LoginService;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $loginService = new LoginService();
        $getLoginRequest = new GetLoginRequest();
        $getLoginRequest->setEmail($request->email);
        $getLoginRequest->setPassword($request->password);
        return $loginService->login($getLoginRequest);
    }

    public function logout(Request $request)
    {
        $loginService = new LoginService();
        return $loginService->logout($request);
    }

    public function updateUserAuth(Request $request)
    {
        $loginService = new LoginService();
        return $loginService->updateUserAuth($request);
    }
}
