<?php

namespace App\Http\Controllers;

use App\Http\Request\GetProdukByKodeRequest;
use App\Http\Request\StoreProdukRequest;
use App\Service\ProdukService;
use Illuminate\Http\Request;

class ProdukController extends Controller
{
    public function getProdukList()
    {
        $produkService = new ProdukService();
        return $produkService->getProdukList();
    }

    public function storeProduk(Request $request)
    {
        $produkService = new ProdukService();
        $storeProdukReqeust = new StoreProdukRequest();
        $storeProdukReqeust->setNama($request->nama);
        $storeProdukReqeust->setMerk($request->merk);
        $storeProdukReqeust->setHarga($request->harga);
        return $produkService->storeProduk($storeProdukReqeust);
    }

    public function updateProduk(Request $request)
    {
        $produkService = new ProdukService();
        $storeProdukReqeust = new StoreProdukRequest();
        $storeProdukReqeust->setKode($request->kode);
        $storeProdukReqeust->setNama($request->nama);
        $storeProdukReqeust->setMerk($request->merk);
        $storeProdukReqeust->setHarga($request->harga);
        return $produkService->updateProduk($storeProdukReqeust);
    }

    public function deleteProdukByKode(Request $request)
    {
        $produkService = new ProdukService();
        $deleteProdukRequest = new GetProdukByKodeRequest();
        $deleteProdukRequest->setKode($request->kode);
        return $produkService->deleteProdukByKode($deleteProdukRequest);
    }

    public function getProdukByKode(Request $request)
    {
        $produkService = new ProdukService();
        $getProdukRequest = new GetProdukByKodeRequest();
        $getProdukRequest->setKode($request->kode);
        return $produkService->getProdukByKode($getProdukRequest);
    }
}
