<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    use HasFactory;

    protected $table = 'produk';
    public $timestamps = false;
    const UPDATED_AT = false;
    const CREATED_AT = false;
    
    protected $fillable = [
        'kode',
        'nama',
        'merk',
        'harga'
    ];
}
