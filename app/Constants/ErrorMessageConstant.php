<?php

namespace App\Constants;

class ErrorMessageConstant
{
    const DATA_NOT_FOUND = "Data Not Found";
    const SOMETHING_WENT_WRONG = "Something Went Wrong";
    const BAD_REQUEST = "Bad Request";
    const LOGIN_FAILED = "These credentials do not match our records";
    const ALREADY_LOGIN = "User Already Login";
}
