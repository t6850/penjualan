<?php

namespace App\Constants;

class ErrorCodeConstant
{
    const DATA_NOT_FOUND = "1001";
    const SOMETHING_WENT_WRONG = "1002";
    const BAD_REQUEST = "1003";
    const LOGIN_FAILED = "1004";
    const ALREADY_LOGIN = "1005";
}
