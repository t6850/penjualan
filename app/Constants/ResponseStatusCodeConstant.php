<?php

namespace App\Constants;

class ResponseStatusCodeConstant
{
    const CONTINUE = 100;
    const SWITCHING_PROTOCOLS = 101;
    const EARLY_HINTS = 103;
    const OK = 200;
    const CREATED = 201;
    const ACCEPTED = 202;
    const NON_AUTHORITATIVE_INFORMATION = 203;
    const NO_CONTENT = 204;
    const RESET_CONTENT = 205;
    const PARTIAL_CONTENT = 206;
    const MULTIPLE_CHOICES = 300;
    const REDIRECT = 301;
    const SEE_OTHER = 303;
    const TEMPORARY_REDIRECT = 307;
    const PERMANENT_REDIRECT = 308;
    const BAD_REQUEST = 400;
    const UNAUTHORIZED = 401;
    const PAYMENT_REQUIRED = 402;
    const FORBIDDEN = 403;
    const NOT_FOUND = 404;
    const METHOD_NOT_ALLOWED = 405;
    const TIME_OUT = 408;
    const INTERNAL_SERVER_ERROR = 500;
}
